
settings_environment = {
    "time_step": 0.25,
    "gravity": 9.81,
    "length": 1,
    "mass": 1,
    "damping": 0.05,
    "phi_init": 0,
    "phi_dot_init": 0
}