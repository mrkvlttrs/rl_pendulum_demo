
import numpy as np
import scipy.integrate as sp_int


class Environment(object):
    """
    Hanging pendulum
    Right handed coordinates system: counter-clockwise rotation is positive.
    Hanging down corresponds to phi = 0 radian.

    Equations of motion:
    phi_ddot * mass * length**2 = (moment - 
        mass * gravity * length * sin(phi) -
        damping * phi_dot)

    Which can be split into two first-order differential equations.
    
    True (continuous) environment, which cannot be fully observed by the agent.
    """

    def __init__(self, settings):
        """
        =INPUT=
            settings - dictionary from settings.py
        """
        self.settings = settings

        self.time_step = settings['time_step']
        self.gravity = settings['gravity']
        self.length = settings['length']
        self.mass = settings['mass']
        self.damping = settings['damping']
        
        self.state = np.array([
            settings["phi_init"], 
            settings["phi_dot_init"]],
            dtype='float')
        return


    def step(self, action):
        """
        Perform a single simulation step using the equations of motion.
        Integration is done with Runge-Kutta 3(2)
        """

        # Call the solver for a single time step
        solution = sp_int.solve_ivp(
            self.state_equations,
            (0.0, self.time_step),
            self.state,
            args=(action,),
            method=sp_int.RK23,
            t_eval=(self.time_step,))

        self.state[:] = solution.y.ravel()
        return


    def state_equations(self, time, state, action):
        """
        To be called by the ODE solver. The first two inputs must be 
        time and state. The rest are additional args.
        """
        
        # Unpack the state variable
        phi, phi_dot = state
        
        # Plug into the equations of motion
        eq1 = phi_dot
        eq2 = ((action - 
            self.mass * self.gravity * self.length * np.sin(phi) - 
            self.damping * phi_dot) / 
            (self.mass * self.length**2))

        return [eq1, eq2]


    def get_state(self):
        return self.state
    

    def set_state(self, state):
        """
        =INPUT=
            state - ndarray of shape (2,)
        Override the state of the model.
        Allows the agent to pick a random state at episode start.
        """
        if state.shape != self.state.shape:
            raise ValueError("Incorrect dimensions input state.")
        self.state[:] = state
        return
    

    



