
import numpy as np
import scipy.integrate as sp_int

#TODO: This is currently not functional / not tested.

class Grid(object):
    """
    Grid world
    """

    def __init__(self, settings):
        """
        =INPUT=
            settings - dictionary from settings.py
        """
        self.settings = settings

        self.max_x = 3
        self.max_y = 4

        self.time_step = settings['time_step']
        self.state = np.array([0, 0], dtype='float')
        
        return


    def step(self, action):
        """
        Perform a single simulation step
        """

        # Single time step update
        self.state[:] = self.state_equations(action)
        return


    def state_equations(self, action):
        return self.state + action

    
    def set_state(self, state):
        """
        Override the state of the model.
        Allows the agent to pick a random state at episode start.
        """
        self.state[:] = state
        return


class GridEnvironment(object):
    
    def __init__(self, settings, grid):

        self.settings = settings
        self.grid = grid

        self.target = np.array([1, 1])

        # Matrix with all observable discretized states
        n_states = grid.max_x * grid.max_y
        self.states = np.zeros((n_states, 2), dtype='int')
        x_steps = np.arange(0, grid.max_x)
        y_steps = np.arange(0, grid.max_y)
        for idx in range(0, grid.max_y):
            idx_start = idx * x_steps.size
            idx_stop = idx_start + x_steps.size
            self.states[idx_start:idx_stop, 0] = x_steps
            self.states[idx_start:idx_stop, 1] = y_steps[idx]

        self.base_actions = np.array([
            [-1, -1], [-1, 0], [-1, 1],
            [0, -1], [0, 0], [0, 1],
            [1, -1], [1, 0], [1, 1]])
        
        self.state_cost = 5 * np.ones((self.states.shape[0],), dtype='int')
        self.state_cost[5] = 0
        self.action_cost = np.any(self.base_actions, axis=1).astype('int')
        return


    @property
    def state(self):
        return self.grid.state


    def set_state(self, state):
        """
        Wrapper
        """
        self.grid.set_state(state)
        return


    def execute(self, action):
        self.grid.step(action)
        return


    def possible_actions(self):
        """
        Returns indices to self.base_actions
        """

        mask = np.zeros((self.base_actions.shape[0],), dtype='bool')

        # Action constraints due world boundaries
        if self.state[0] == 0:
            mask = mask & self.base_actions[:, 0] != -1
        elif self.state[0] == self.grid.max_x - 1:
            mask = mask & self.base_actions[:, 0] != 1
        if self.state[1] == 0:
            mask = mask & self.base_actions[:, 1] != -1
        elif self.state[1] == self.grid.max_y - 1:
            mask = mask & self.base_actions[:, 1] != 1

        # Action constraints due to barriers
        if (self.state[0] == 0 and (self.state[1] == 0 or 
                self.state[1] == 1 or self.state[1] == 2)):
            mask = mask & self.base_actions[:, 0] != 1

        return np.arange(0, self.base_actions.shape[0])[mask]


    def reward_function(self, state, action):

        idx_state = np.argmin(np.sum(abs(self.states - state), axis=1))
        idx_action = np.argmin(np.sum(abs(self.base_actions - action), axis=1))
        
        total_reward = - self.state_cost[idx_state]  - self.action_cost[idx_action]

        return total_reward


    def state_index(self, state=None):
        if state is None:
            state = self.state
        return np.argmin(self.states - self.state)


    def is_in_target(self):
        return self.state == self.target