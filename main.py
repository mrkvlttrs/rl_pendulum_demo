import time

from environments.pendulum import Environment
from environments.pendulum_config import settings_environment

from interfaces.pendulum import EnvironmentInterface, ObjectiveFunction
from interfaces.pendulum_config import settings_interface, settings_objective

from agents.dp import Agent
from agents.dp_config import settings_agent

from visualization import value_map, policy_map, compute_path


# Run agent-environment combination
environment = Environment(settings_environment)
interface = EnvironmentInterface(settings_interface, environment)
objective_function = ObjectiveFunction(settings_objective)
agent = Agent(settings_agent, interface, objective_function)

t_start = time.time()
agent.run()
print(f"Total runtime: {time.time() - t_start} s.")

# Visualize results
if hasattr(agent, "v_values"):
    value_map(agent, interface)

if hasattr(agent, "policy"):
    path = compute_path(agent, interface, interface.get_state(3825), 40)
    policy_map(agent, interface, path)
