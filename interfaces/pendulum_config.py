import numpy as np


settings_interface = {
    "n_actions": 3,
    "action_max": 9.81 * np.sin(0.15 * np.pi),
    
    "n_variables_observed": 2,
    "phi_min": 0,
    "phi_max": 2 * np.pi,
    "phi_steps": 51, #11, 51

    "phi_dot_min": -3 * np.pi,
    "phi_dot_max": 3 * np.pi,
    "phi_dot_steps": 151, #31, 151
}


settings_objective = {
    "phi_target": np.pi,
    "phi_dot_target": 0,
    "phi_weight": 10,
    "phi_dot_weight": 0.5,
    "action_weight": 0.05
}