import random
import numpy as np


class EnvironmentInterface(object):
    """
    The observable environment, i.e. the interface between the agent and the
    true pendulum environment.
    """

    def __init__(self, settings, environment):
        """
        """
        self.settings = settings
        
        self.n_actions = settings["n_actions"]
        self.n_variables_observed = settings["n_variables_observed"]

        self.phi_min = settings["phi_min"]
        self.phi_max = settings["phi_max"]
        self.phi_steps = settings["phi_steps"]

        self.phi_dot_min = settings["phi_dot_min"]
        self.phi_dot_max = settings["phi_dot_max"]
        self.phi_dot_steps = settings["phi_dot_steps"]

        self.action_max = settings["action_max"]

        self.environment = environment

        # Possible observable states
        self.states = np.zeros((self.phi_steps * self.phi_dot_steps, self.n_variables_observed), dtype='float')
        phi_discrete = np.linspace(self.phi_min, self.phi_max, self.phi_steps)
        phi_dot_discrete = np.linspace(self.phi_dot_min, self.phi_dot_max, self.phi_dot_steps)
        for idx in range(0, self.phi_dot_steps):
            idx_start = idx * self.phi_steps
            idx_stop = idx_start + self.phi_steps
            self.states[idx_start:idx_stop, 0] = phi_discrete
            self.states[idx_start:idx_stop, 1] = phi_dot_discrete[idx]

        # All possible actions for each state (here independent of state)
        self.actions = np.array([-1, 0, 1]) * self.action_max
        return


    def get_state(self, idx_state=None):
        """
        Discrete version of underlying continuous state
        
        =INPUT=
            [idx_state] - integer or NoneType
                If None, return current state
                If int >= 0, return state at that index
                If -1, return a random state
                Defaults to None
        """
        if not idx_state:
            return self.states[self.state_index(), :]
        elif idx_state < 0:
            return self.states[random.randint(0, self.states.shape[0] - 1)]
        return self.states[idx_state, :]


    def set_state(self, state):
        """
        Wrapper
        """
        self.environment.set_state(state)
        return


    def get_actions(self, state=None):
        """
        =NOTES=
        For this environment, possible actions are independent of the state.
        """
        return self.actions


    def step(self, action):
        """
        Wrapper
        """
        self.environment.step(action)
        return


    def state_index(self, state=None):
        """
        Discretize the true state and get the corresponding index in the states
        array.

        =INPUT=
            state - ndarray of shape (2,)
                Environment state
        """
        if state is None:
            phi, phi_dot = self.environment.state
        else:
            phi, phi_dot = state
        
        return np.argmin(
            abs(self.states[:, 0] - phi % (2 * np.pi)) + 
            abs(self.states[:, 1] - phi_dot))

        

class ObjectiveFunction(object):
    
    def __init__(self, settings):
        self.phi_target = settings["phi_target"]
        self.phi_dot_target = settings["phi_dot_target"]
        self.phi_weight = settings["phi_weight"]
        self.phi_dot_weight = settings["phi_dot_weight"]
        self.action_weight = settings["action_weight"]
        return
    

    def evaluate(self, state, action):
        """
        Reward which the agent receives for a given state-action pair
        
        =INPUT=
            state - ndarray of shape (2,)
                Environment state containing phi and phi_dot
            action - float
                Action that was taken to get into state. A moment in Nm.
        =NOTES=
            Rewards are negative costs, costs are negative rewards.
        """
        # Unpack the state
        phi, phi_dot = state

        # Cost on angle
        phi_penalty = (phi - np.pi)**2

        # Cost on (extreme) velocities
        phi_dot_penalty = phi_dot**2

        # Cost on taking actions
        action_penalty = action**2

        total_reward = (
            - self.phi_weight * phi_penalty 
            - self.phi_dot_weight * phi_dot_penalty 
            - self.action_weight * action_penalty)

        return total_reward
