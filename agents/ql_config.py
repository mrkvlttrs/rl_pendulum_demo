settings_agent = {
    'n_episodes': 300,
    'steps_per_episode': 1000,
    'learn_rate': 0, # Is currently overwritten in the agent...
    'discount_factor': 0.95,
    'exploration_rate': 0.75
}