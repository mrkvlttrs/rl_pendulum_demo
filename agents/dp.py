import numpy as np


class Agent(object):
    """
    Dynamic programming agent.
    States and actions discrete and deterministic.
    """

    def __init__(self, settings, interface, obj_fun):
        
        self.settings = settings
        
        self.interface = interface
        self.obj_fun = obj_fun

        n_states = interface.states.shape[0]
        n_actions = interface.actions.size

        self.r_values = np.zeros((n_states, n_actions), dtype="float")
        self.q_values = np.zeros((n_states, n_actions), dtype="float")
        self.v_values = np.zeros((n_states, ), dtype="float")
        self.policy = np.nan * np.ones((n_states, ), dtype="float")
        self.adjacency = -1 * np.ones((n_states, n_actions), dtype=int)

        return


    def run(self):
        
        print("Collecting rewards for all known state-action combinations...")

        # Traverse all possible state-action combinations to observe their
        # immediate rewards.
        for idx_state in range(0, self.q_values.shape[0]):
            current_state = self.interface.get_state(idx_state)
            possible_actions = self.interface.get_actions(idx_state)

            for idx_action in range(0, self.q_values.shape[1]):
                try:
                    action = possible_actions[idx_action]
                except:
                    # Skip if action not available
                    continue

                # Simulate one step
                self.interface.set_state(current_state)
                self.interface.step(action)
                idx_state_new = self.interface.state_index()
                new_state = self.interface.get_state()
                self.adjacency[idx_state, idx_action] = idx_state_new

                # Store immediate reward
                self.r_values[idx_state, idx_action] = (
                    self.obj_fun.evaluate(new_state, action))

        # Perform value iteration
        self.compute_v_values()
        return
    

    def compute_v_values(self, max_iter=10**4):

        # Use the adjacency list as index to the optimal values per state
        n_iter = 0
        while n_iter < max_iter:
            q_values_new = np.zeros_like(self.q_values)
            for idx_action in range(0, self.q_values.shape[1]):
                
                update = (self.r_values[:, idx_action] +
                    self.v_values[self.adjacency[:, idx_action]])
                mask = self.adjacency[:, idx_action] >= 0
                q_values_new[:, idx_action][mask] = update[mask]

            v_values_new = np.max(q_values_new, axis=1)
            policy_new = np.argmax(q_values_new, axis=1)

            if np.all(self.policy == policy_new):
                print(f"Converged policy after {n_iter} iterations.")
                self.q_values = q_values_new
                self.v_values = v_values_new
                self.policy = policy_new
                return
            self.v_values = v_values_new
            self.policy = policy_new

            print(f'Completed iteration: {n_iter}')
            n_iter += 1

        return