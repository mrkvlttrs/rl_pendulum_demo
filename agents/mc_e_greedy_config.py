settings_agent = {
    "n_episodes": 3000,
    "steps_per_episode": 50,
    "discount_factor": 0.8,
    "epsilon": 0.4
}