import random
import numpy as np


class Agent(object):
    """
    Dynamic programming agent.
    States and actions discrete and deterministic.
    """

    def __init__(self, settings, interface, obj_fun):
        
        self.settings = settings
        self.interface = interface
        self.obj_fun = obj_fun

        self.n_episodes = settings['n_episodes']
        self.steps_per_episode = settings['steps_per_episode']
        self.learn_rate = settings['learn_rate']
        self.discount_factor = settings['discount_factor']
        self.exploration_rate = settings['exploration_rate']

        n_states = interface.states.shape[0]
        n_actions = interface.actions.size

        # self.r_values = np.zeros((n_states, n_actions), dtype="float")
        self.q_values = np.zeros((n_states, n_actions), dtype="float")
        self.v_values = np.zeros((n_states, ), dtype="float")
        self.policy = np.argmax(self.q_values, axis=1)
        self.adjacency = -1 * np.ones((n_states, n_actions), dtype=int)

        return


    def run(self):
        
        for idx_episode in range(0, self.n_episodes):
            self.update_learn_rate(idx_episode)

            # Generate random starting point
            rand_state = self.interface.get_state(-1)
            self.interface.set_state(rand_state)
            self.run_episode()

            self.v_values = np.max(self.q_values, axis=1)
            self.policy = np.argmax(self.q_values, axis=1)

            print(f"Completed episode {idx_episode}.")

        return
    

    def run_episode(self):
        """
        Have the agent execute an episode of steps_per_episode steps on
        the environment.
        =INPUT=
            interface - object
        =NOTES=
        """

        for idx_step in range(0, self.steps_per_episode):
            idx_state = self.interface.state_index()
            possible_actions = self.interface.get_actions()
            
            # Use epsylon-greedy strategy to choose exploration or exploitation.
            epsylon =  self.exploration_rate * (1 - idx_step / self.steps_per_episode)
            if random.random() <= epsylon:
                # Exploration
                idx_action = random.randint(0, possible_actions.size - 1)
            else:
                # Exploitation
                idx_action = self.policy[idx_state]
            
            # Execute the action in the environment to move to a new state
            action = possible_actions[idx_action]
            self.interface.step(action)
            idx_state_new = self.interface.state_index()
            new_state = self.interface.get_state()
            self.adjacency[idx_state, idx_action] = idx_state_new

            # Compute reward of moving to the new state
            reward = self.obj_fun.evaluate(new_state, action)

            # Obtain the optimal value for the new state (max possible reward)
            v_value = np.max(self.q_values[idx_state_new, :])

            # Update q-table with new q-value for state where agent came from
            q_current = self.q_values[idx_state, idx_action]
            q_new = reward + self.discount_factor * v_value
            self.q_values[idx_state, idx_action] = (
                (1 - self.learn_rate) * q_current + self.learn_rate * q_new)

            # Terminate prematurely desired state has been reached
            if abs(self.obj_fun.evaluate(new_state, action)) < 10**-9:
                print('Target reached, episode terminated.')
                break

        return


    def update_learn_rate(self, idx_episode):
        """
        Create an episode-dependent learning rate, which decays with episode
        number. This prevents "aggressively overwriting" previously learned
        knowledge, especially in the later stages.
        =INPUT=
            idx_episode - int
                Episode number (0-based)
        """
        self.learn_rate = 0.9 - 0.85 * idx_episode / self.n_episodes
        return