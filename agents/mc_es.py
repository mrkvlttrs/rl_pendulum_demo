import numpy as np


class Agent(object):
    """
    Dynamic programming agent.
    States and actions discrete and deterministic.
    """

    def __init__(self, settings, interface, obj_fun):
        
        self.settings = settings
        self.interface = interface
        self.obj_fun = obj_fun

        self.n_episodes = settings['n_episodes']
        self.steps_per_episode = settings['steps_per_episode']
        self.discount_factor = settings['discount_factor']

        n_states = interface.states.shape[0]
        n_actions = interface.actions.size

        self.return_sum = np.zeros((n_states, n_actions), dtype="float")
        self.n_returns = np.zeros_like(self.return_sum, dtype="int")
        self.q_values = np.random.rand(n_states, n_actions)
        self.v_values = np.zeros((n_states, ), dtype="float")
        self.policy = np.argmax(self.q_values, axis=1)
        self.adjacency = -1 * np.ones((n_states, n_actions), dtype=int)

        return


    def run(self):
        
        for idx_episode in range(0, self.n_episodes):

            self.run_episode()

            mask = self.n_returns != 0
            self.q_values[mask] = (self.return_sum[mask] / self.n_returns[mask])
            self.policy = np.argmax(self.q_values, axis=1)

            print(f"Completed episode {idx_episode}.")

            # Generate new random starting point for next episode
            rand_state = self.interface.get_state(-1)
            self.interface.set_state(rand_state)

        self.v_values = np.max(self.q_values, axis=1)

        return
    

    def run_episode(self):
        """
        Have the agent execute an episode of steps_per_episode steps on
        the environment.
        =INPUT=
            interface - object
        =NOTES=
        """

        g_sequence = []
        visited_s_a = []
        for _ in range(0, self.steps_per_episode):
            idx_state = self.interface.state_index()
            possible_actions = self.interface.get_actions()
            
            # Use policy to select action
            idx_action = self.policy[idx_state]
            action = possible_actions[idx_action]

            # Execute the action in the environment to move to a new state
            self.interface.step(action)
            idx_state_new = self.interface.state_index()
            new_state = self.interface.get_state()
            self.adjacency[idx_state, idx_action] = idx_state_new

            # Compute reward of moving to the new state
            reward = self.obj_fun.evaluate(new_state, action)
            g_sequence.append(reward)
            visited_s_a.append((idx_state, idx_action))

        # Traverse the sequence backward to compute returns
        g_sequence.reverse()
        visited_s_a.reverse()
        for idx in range(1, len(g_sequence)):
            g_sequence[idx] =  self.discount_factor * g_sequence[idx - 1] + g_sequence[idx]

            if visited_s_a[idx] not in visited_s_a[idx + 1::]:
                idx_state, idx_action = visited_s_a[idx]
                self.return_sum[idx_state, idx_action] += g_sequence[idx]
                self.n_returns[idx_state, idx_action] += 1
        
        return

