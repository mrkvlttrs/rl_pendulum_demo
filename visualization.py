
import numpy as np
from matplotlib import pyplot as plt


def value_map(agent, interface, path=None):

    v_values = agent.v_values.copy()
    v_values.shape = (-1, interface.phi_steps)

    fig = plt.figure(0)
    ax = fig.add_subplot(1, 1, 1)
    img = ax.imshow(v_values, origin='lower')
    if path is not None:
        path_x_data = (path % interface.phi_steps).astype('int')
        path_y_data = np.floor(path / interface.phi_steps).astype('int')
        ax.plot(path_x_data, path_y_data, '-om', markersize=3)

    ax.set_xlabel('Angle (rad)')
    ax.set_ylabel('Angular velocity (rad/s)')
    ax.set_title('Value map')
    ax.set_xticks([0, interface.phi_steps])
    ax.set_xticklabels([round(interface.phi_min, 2), round(interface.phi_max, 2)])
    ax.set_yticks([0, interface.phi_dot_steps])
    ax.set_yticklabels([round(interface.phi_dot_min, 2), round(interface.phi_dot_max, 2)])

    fig.colorbar(img, ax=ax)

    fig.show()

    return


def policy_map(agent, interface, path=None):
    """
    Path - ndarray of shape (N,)
        Indices from the interface.state matrix
    """

    policy = agent.policy.copy()
    policy.shape = (-1, interface.phi_steps)

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(policy, origin='lower')
    if path is not None:
        path_x_data = (path % interface.phi_steps).astype('int')
        path_y_data = np.floor(path / interface.phi_steps).astype('int')
        ax.plot(path_x_data, path_y_data, '-om', markersize=3)

    ax.set_xlabel('Angle (rad)')
    ax.set_ylabel('Angular velocity (rad/s)')
    ax.set_title('Policy map')
    ax.set_xticks([0, interface.phi_steps])
    ax.set_xticklabels([round(interface.phi_min, 2), round(interface.phi_max, 2)])
    ax.set_yticks([0, interface.phi_dot_steps])
    ax.set_yticklabels([round(interface.phi_dot_min, 2), round(interface.phi_dot_max, 2)])

    fig.show()

    return



def compute_path(agent, interface, state, n_steps=1):
    """
    state - ndarray
        State to begin in
    """

    idx_path = []
    interface.set_state(state)
    idx_step = 0
    while idx_step < n_steps:
        idx_state = interface.state_index()
        if len(idx_path) and idx_path[-1] == idx_state:
            break
        
        possible_actions = interface.get_actions(idx_state)
        action = possible_actions[agent.policy[idx_state]]
        interface.step(action)
        idx_path.append(idx_state)
        idx_step += 1

    return np.array(idx_path, dtype='int')